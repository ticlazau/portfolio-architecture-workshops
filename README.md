Workshop online at: [https://redhatdemocentral.gitlab.io/portfolio-architecture-workshops](https://redhatdemocentral.gitlab.io/portfolio-architecture-workshops)

![Cover Slide](cover.png)
![Logical diagram](images/rhte-lab3-43.png)
![Schematic diagram](images/rhte-lab5-63.png)
![Detail diagram](images/rhte-lab6-27.png)

